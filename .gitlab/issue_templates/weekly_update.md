All Weekly Demos: https://gitlab.com/gitlab-org/incubation-engineering/mlops/meta/-/issues/16

## Recording

Soon

## Vision

Make GitLab a tool Data Scientists and Machine Learning Engineers love to use.

## Mission
Explore and Collaborate with different teams to deliver features that improve the user experience for Data Scientists and Machine Learning Engineers, while increasing awareness within the company to this user groups

## What Was Done



## Up Next




/label ~SingleEngineerGroups ~"incubation::mlops" ~"incubation::mlops::updates" ~"Category::MLOps" 
